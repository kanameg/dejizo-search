(require 'dejizo-search)
(define-key global-map (kbd "C-c C-w") 'dejizo-search-current)
(define-key global-map (kbd "C-c C-r") 'dejizo-search-region)
