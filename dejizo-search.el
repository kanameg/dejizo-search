;; -*- Emacs-Lisp -*-

;; dejizo-search.el ---

;; Copyright (C) 2014 YOSHIDA Kaname
;; Author: YOSHDIA Kaname <kaname.g@gmail.com>

;; (require 'dejizo-search)

(require 'url)
(require 'xml)
(require 'pos-tip)

;; ------------------------------------------
;; 定数定義
;; ------------------------------------------

;; バージョン
(defconst dejizo-search-version "1.0")

;; EJdict検索用の定義
(defconst dejizo-search-server "public.dejizo.jp")
(defconst dejizo-search-method "NetDicV09.asmx/SearchDicItemLite")
(defconst dejizo-search-params '((Dic . EJdict) (Scope . HEADWORD)
				 (Match . EXACT) (Merge . OR) (Prof . XHTML)
				 (PageSize . 20) (PageIndex . 0)))
;; EJdict内容取得用の定義
(defconst dejizo-get-server "public.dejizo.jp")
(defconst dejizo-get-method "NetDicV09.asmx/GetDicItemLite")
(defconst dejizo-get-params '((Dic . EJdict) (Loc . "")
				 (Prof . XHTML)))

(defgroup dejizo-search nil
  "dejizo-search"
  :group 'emacs)


;; ------------------------------------------
;; 設定定義
;; ------------------------------------------
(defvar dejizo-display-use-popup nil
  "Non-nil means a using pop-up window.")

(defvar dejizo-proxy-server nil
  "Non-nil means a HTTP access will use proxy server name or IP address.")

(defvar dejizo-proxy-port nil
  "HTTP proxt access will use this port.")


;; ------------------------------------------
;; ユーティリティ関数
;; ------------------------------------------

(defun dejizo-url-encode (str &optional coding-system)
  "文字列をURLエンコード"
  (if (stringp str)
      (if (or (null coding-system)
	      (not (coding-system-p coding-system)))
	  (setq coding-system 'utf-8))
    (mapconcat
     (lambda (c)
       (cond
	((or (and (<= ?A c) (<= c ?z))
	     (and (<= ?0 c) (<= c ?9))
	     (eq c ?.)
	     (eq c ?-)
	     (eq c ?_)
	     (eq c ?~))
	 (char-to-string c))
	(t
	 (format "%%%X" c))))
     (encode-coding-string str coding-system)
     "")
    str))


(defun dejizo-make-url (server method &optional params)
  "デ辞蔵検索URLを生成 (単語検索 意味検索)"
  (concat "http://" server "/" method
	  (when params
	    (concat "?"
		    (mapconcat
		     (lambda (p)
		       (format "%s=%s" (car p) (cdr p)))
		     params "&")))))



;; ------------------------------------------
;; バッファ操作関連
;; ------------------------------------------

;; 検索結果表示用のバッファ関連定義
(defconst dejizo-result-buffer "*dejizo-result-buffer*")


(defun dejizo-get-result-buffer ()
  "デ辞蔵検索結果表示用のバッファを取得"
  (let ((buffer dejizo-result-buffer))
    (dejizo-get-or-generate-result-buffer buffer)))


(defun dejizo-get-or-generate-result-buffer (buffer)
  "デ辞蔵検索結果表示用バッファの取得または生成"
  (if (stringp buffer)
      (or (get-buffer buffer)
	  (generate-new-buffer buffer))))



;; ------------------------------------------
;; IDから単語の意味を取得
;; ------------------------------------------

(defun dejizo-retrieve-synchronously (url)
  "URLへHTTPアクセスを送信し、同期で受信した文字列を返す"
  (let* ((buffer (url-retrieve-synchronously url)))
    (set-buffer buffer)
    (buffer-string)))


(defun dejizo-get-url-synchronously (url)
  "単語意味 取得URLをHTTP同期通信で送信"
  (let* ((response (dejizo-retrieve-synchronously url))
	 (xml (substring response
	 		 (+ (string-match "\r?\n\r?\n" response)
	 		    (length (match-string 0 response)))
	  		 (1- (point-max))))
	 (root (car (with-temp-buffer
	 	      (insert xml)
	 	      (xml-parse-region (point-min) (point-max)))))
	 (text (dejizo-get-and-decode-text root))
	 (word (dejizo-get-word root)))
    (cons word text)))


(defun dejizo-get-ID-mean (ID)
  "IDから単語意味を検索して単語と意味の対(consセル)を取得"
  (let* ((params
	  (append dejizo-get-params `((Item . ,ID))))
	 (url
	  (dejizo-make-url dejizo-get-server
			   dejizo-get-method params)))
    (dejizo-get-url-synchronously url)))


(defun dejizo-format-result-string (results)
  "検索結果文字列から、バッファ表示用の文字列を生成"
  (mapconcat (lambda (result)
	       (format "* %s\n%s" (car result)
		       (mapconcat
			(lambda (mean)
			  (format " - %s" mean))
			(split-string (cdr result) "\t")
			"\n")))
	     results "\n"))


(defun dejizo-display-result-buffer (string)
  "検索結果バッファを取得し結果文字列を表示"
  (save-excursion
    (set-buffer (dejizo-get-result-buffer))
    (setq buffer-read-only nil)
    (erase-buffer)
    (insert string)
    (goto-char (point-min))
    (setq buffer-read-only t)))


(defun dejizo-get-and-display-IDs-mean (IDs)
  "IDリストから意味を検索し、検索結果バッファに表示"
  (let* ((result-string (dejizo-format-result-string
			 (mapcar 'dejizo-get-ID-mean IDs))))
    (cond (dejizo-display-use-popup
	   (pos-tip-show result-string))
	  (t
	   (dejizo-display-result-buffer result-string)
	   (fit-window-to-buffer
	    (display-buffer (dejizo-get-result-buffer))
	    10 10)))))



;; ------------------------------------------
;; 単語からIDを検索
;; ------------------------------------------

(defun dejizo-search-word (word)
  "単語を指定して、一致した単語のIDリストを取得"
  (let* ((params
	  (append dejizo-search-params `((Word . ,word))))
	 (url
	  (dejizo-make-url dejizo-search-server
			   dejizo-search-method params)))
    (url-retrieve url #'dejizo-search-sentinel)))


;;(defun dejizo-search-url (url)
;;  "単語の検索URLをHTTPで非同期送信"
;;  (url-retrieve url #'dejizo-search-sentinel))


(defun dejizo-search-sentinel (status)
  "単語検索完了(終端)処理"
  (let* ((response (buffer-string))
	 (xml (substring response
			 (+ (string-match "\r?\n\r?\n" response)
			    (length (match-string 0 response)))
			 (1- (point-max))))
	 (root (car (with-temp-buffer
		      (insert xml)
		      (xml-parse-region (point-min) (point-max)))))
	 (IDs (dejizo-search-result-IDs root)))
    ;;(message "Search result: %s" IDs)
    (message "Search result: %d item found" (length IDs))
    ;; 検索結果が0でなければ 続けてIDで辞書引きを行う
    (when (not (equal (length IDs) 0))
      (dejizo-get-and-display-IDs-mean IDs))))

;; ------------------------------------------
;; 検索結果 XML関連 (XMLの構成に依存)
;; ------------------------------------------

(defun dejizo-search-result-IDs (xml-root)
  "検索結果ID一覧をリストで取得"
  (let* ((items (xml-get-children
		 (car (xml-get-children xml-root 'TitleList))
		 'DicItemTitle)))
    (mapcar (lambda (x)
	      (car (xml-node-children
		    (car (xml-get-children x 'ItemID)))))
	    items)))


(defun dejizo-get-and-decode-text (xml-root)
  "単語意味検索結果XMLから意味部分を取得(全角->半角変換, UTF-8デコード)"
  (japanese-hankaku (decode-coding-string 
		     (car (xml-node-children
			   (car (xml-get-children
				 (car (xml-get-children
				       (car (xml-get-children xml-root 'Body))
				       'div))
				 'div))))
		     'utf-8) t))


(defun dejizo-get-word (xml-root)
  "単語意味検索結果XMLから単語部分を取得"
  (car (xml-node-children
	(car (xml-get-children
	      (car (xml-get-children
		    (car (xml-get-children xml-root 'Head))
		    'div))
	      'span))
	)))


;; ------------------------------------------
;; コマンドの定義(インタラクティブ関数)
;; ------------------------------------------

(defun dejizo-search-region (&optional begin end)
  "リージョンで選択された単語を検索"
  (interactive "r")
  (save-excursion 
    (if (region-active-p)
	(dejizo-search-word (buffer-substring begin end)))))


(defun dejizo-search ()
  "対話形式でミニバッファに入力した単語を検索"
  (interactive)
  (save-excursion 
    (let ((word (read-from-minibuffer "search word?: " nil)))
      (dejizo-search-word word))))


(defun dejizo-search-current ()
  "カーソル位置の単語を検索"
  (interactive)
  (save-excursion 
    (let ((end (progn (forward-word) (point)))
	  (begin (progn (backward-word) (point))))
      (dejizo-search-word (buffer-substring begin end)))))


(defun dejizo-search-version ()
  "バージョン表示"
  (interactive)
  (save-excursion 
    (message "Dejizo search %s" dejizo-search-version)))

(provide 'dejizo-search)
;; end
